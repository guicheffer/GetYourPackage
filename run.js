const { config } = require('./package.json')
const rawData = require('./data.json')

const express = require('express')
const path = require('path')
const PORT = process.env.PORT || config.port || 8080

const app = express(),
      staticServe = express.static(path.join(__dirname, config.path.dist))

app.use('/tours', (req, res) => {
  const { title: rawTitle, priceFrom: rawPriceFrom, priceTo: rawPriceTo } = req.query
  const [title, priceFrom, priceTo] = [
    rawTitle || '',
    parseFloat(rawPriceFrom) || 0,
    parseFloat(rawPriceTo) || 0,
  ]

  const tours =
    rawData.tours.filter((tour) => {
      // Filter by price
      // ¯\_(ツ)_/¯

      const tourPrice = parseFloat(tour.price)
      if (priceFrom && priceTo) return tourPrice >= priceFrom && tourPrice <= priceTo
      if (priceFrom) return tourPrice >= priceFrom
      if (priceTo) return tourPrice <= priceTo

      return true
    }).filter((tour) => {
      // Filter by title
      // ( ͡° ͜ʖ ͡°)

      if (!title) return true

      const { title: rawTourTitle } = tour
      return rawTourTitle.toLowerCase().indexOf(title.toLowerCase()) !== -1
    })

  res.send({ tours })
})

app.use('/', staticServe)
app.use('*', staticServe)

app.listen(PORT, () => console.log(`Listening on ${ PORT }`))
