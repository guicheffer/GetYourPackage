import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import tours from '../../main/modules/tours'

export default combineReducers({
  tours,
  routing: routerReducer,
})
