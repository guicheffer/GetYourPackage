/*-
 * ⭐️ Particles background
 *
 * This is where GetYourPackage background comes from!
 *
-*/

require('particles.js')

// eslint-disable-next-line no-undef
const browser = window

const OPTIONS = {
  particles: {
    number: {
      value: 52,
      density: {
        enable: true,
        value_area: 561.194221302933,
      },
    },
    color: {
      value: '#ffffff',
    },
    shape: {
      type: 'image',
      stroke: {
        width: 0,
        color: '#000000',
      },
      polygon: {
        nb_sides: 5,
      },
      image: {
        src: '/static/main/airplane-emoji.png',
        width: 100,
        height: 100,
      },
    },
    opacity: {
      value: 1,
      random: true,
      anim: {
        enable: true,
        speed: 1,
        opacity_min: 0,
        sync: false,
      },
    },
    size: {
      value: 7.891476416322726,
      random: true,
      anim: {
        enable: false,
        speed: 4,
        size_min: 0.3,
        sync: false,
      },
    },
    line_linked: {
      enable: false,
      distance: 100,
      color: '#ffffff',
      opacity: 0.2,
      width: 0.6,
    },
    move: {
      enable: true,
      speed: 0.1,
      direction: 'top-right',
      random: false,
      straight: false,
      out_mode: 'out',
      bounce: false,
      attract: {
        enable: false,
        rotateX: 0,
        rotateY: 0,
      },
    },
  },
  interactivity: {
    detect_on: 'canvas',
    events: {
      onhover: {
        enable: false,
        mode: 'bubble',
      },
      onclick: {
        enable: false,
        mode: 'repulse',
      },
      resize: true,
    },
    modes: {
      grab: {
        distance: 400,
        line_linked: {
          opacity: 1,
        },
      },
      bubble: {
        distance: 250,
        size: 0,
        duration: 2,
        opacity: 0,
        speed: 3,
      },
      repulse: {
        distance: 400,
        duration: 0.4,
      },
      push: {
        particles_nb: 4,
      },
      remove: {
        particles_nb: 2,
      },
    },
  },
  retina_detect: true,
}

export default {
  init: () => browser.particlesJS('app-get-your-package', OPTIONS),
}
