import React, { Component } from 'react'
import { List, ListItem, ListSubHeader, ProgressBar } from 'react-toolbox'

import dangerouslySetInnerHTML from '../../../helpers/dangerously-set-inner-html'

class Results extends Component {
  render () {
    return (
      <section className="search__results">
        { !this.props.isLoading ?
            this._createResultsList()
          : <ProgressBar type="circular" mode="indeterminate" />
        }
      </section>
    )
  }

  _createResultsList () {
    return (this.props.results.length ?
        <List className="results__list">
          <ListSubHeader caption={`Available tours (${this.props.results.length})`} />

          {this.props.results.map((result, key) => (
            <ListItem
              className="results__list--result"
              caption={dangerouslySetInnerHTML(result.title)}
              legend={`$ ${result.price}`}
              rightIcon={result.isSpecialOffer ? '❤️' : ''}
              key={key}
            />
          ))}
        </List>
      : <p className="results__empty"> no results found 😞 </p>
    )
  }
}

export default Results
