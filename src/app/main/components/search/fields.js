import React, { Component } from 'react'
import { Button, Input } from 'react-toolbox'

const DEFAULT_MAXLENGTH_INPUT = 24

class Fields extends Component {
  constructor (props) {
    super(props)
    this.state = {
      title: '',
      priceFrom: '',
      priceTo: '',
    }
  }

  render () {
    return (
      <form onSubmit={this._handleSearch.bind(this)} className="search__fields">
        <div className="fields__group">
          <Input
            type="text"
            label="Search for the title..."
            name="title"
            className="fields__title"
            autoFocus
            autoComplete="off"
            value={this.state.title}
            onChange={this._handleChange.bind(this, 'title')}
            maxLength={DEFAULT_MAXLENGTH_INPUT}
          />
        </div>

        <div className="fields__group">
          <Input
            type="number"
            label="...from"
            name="priceFrom"
            className="fields__price-from"
            autoComplete="off"
            value={this.state.priceFrom}
            onChange={this._handleChange.bind(this, 'priceFrom')}
            icon="💰"
          />

          <Input
            type="number"
            label="...to"
            name="priceTo"
            className="fields__price-to"

            autoComplete="off"
            value={this.state.priceTo}
            onChange={this._handleChange.bind(this, 'priceTo')}
            icon="💰"
          />

          <Button
            raised primary
            type="submit"
            className="fields__action"
            label="Search"
            disabled={!this.state.title && !this.state.priceFrom && !this.state.priceTo}
          />
        </div>
      </form>
    )
  }

  _handleSearch (event) {
    const { title, priceFrom, priceTo } = this.state

    this.props.getResults({ title, priceFrom, priceTo })

    event.preventDefault()
  }

  _handleChange (name, value) {
    this.setState({ ...this.state, [name]: value })
  }
}

export default Fields
