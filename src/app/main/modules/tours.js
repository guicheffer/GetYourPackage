import _ from 'lodash'
import axios from 'axios'

import { getEndpointUrl } from '../../helpers/get-initialization-data'

// eslint-disable-next-line no-undef
const browser = window

const DEFAULT_TIMEOUT_SIZE = 1000

export const ACTIVATE_LOADING = 'tours/ACTIVATE_LOADING'
export const LOAD_RESULTS = 'tours/LOAD_RESULTS'

const initialState = {
  isFiltering: false,
  isLoading: true,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTIVATE_LOADING: {
      return { isLoading: true }
    }

    case LOAD_RESULTS: {
      const { tours: list } = action.results.data

      return {
        isFiltering: action.isFiltering,
        isLoading: false,
        list,
      }
    }

    default:
      return state
  }
}

export const getResults = (fields = {}) => {
  const trigger = (dispatch) => {
    const params = _.omitBy(fields, _.isEmpty)

    dispatch({ type: ACTIVATE_LOADING })

    axios.get(getEndpointUrl(), { params })
      .then((results) => {
        // Force a short time (because it loads too fast once we're on the same host)
        browser.setTimeout(() => {
          dispatch({
            type: LOAD_RESULTS,
            results,
            isFiltering: !_.isEmpty(params),
          })
        }, DEFAULT_TIMEOUT_SIZE)
      })
  }

  return trigger
}
