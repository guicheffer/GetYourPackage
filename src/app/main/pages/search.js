import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Button, Switch } from 'react-toolbox'
import { Helmet } from 'react-helmet'

import { getTitle } from '../../helpers/get-initialization-data'

import Fields from '../components/search/fields'
import Results from '../components/search/results'

import { getResults } from '../modules/tours'

class Search extends Component {
  constructor (props) {
    super(props)
    this.state = { onlySpecialOffer: false }
  }

  render () {
    return (
      <div className="panel__search">
        <Helmet>
          <title> {getTitle('Search')} </title>
        </Helmet>

        <Fields getResults={this.props.getResults}/>

        <Switch
          className="search__special-offer--toggle"
          checked={this.state.onlySpecialOffer}
          label="Only special offers ❤️"
          onChange={this._handleChange.bind(this, 'onlySpecialOffer')}
        />

        <Results
          isLoading={this.props.isLoading}
          results={this._getUpdatedResults()}
        />

        {
          this.props.isFiltering ?
            <Button
              raised accent
              label="Load all tours"
              onClick={() => this.props.getResults()}
            />
          : ''
        }
        {
          this.props.results ?
            <p className="search__warning"> ❤️ means its a special offer </p>
          : ''
        }
      </div>
    )
  }

  _getUpdatedResults () {
    if (!this.props.results || !this.props.results.length) return []
    if (!this.state.onlySpecialOffer) return this.props.results

    return this.props.results.filter(result => result.isSpecialOffer)
  }

  _handleChange (name, value) {
    this.setState({ ...this.state, [name]: value })
  }

  componentWillMount () { this.props.getResults() }
}

const mapStateToProps = state => ({
  isFiltering: state.tours.isFiltering,
  isLoading: state.tours.isLoading,
  results: state.tours.list,
})

const mapDispatchToProps = dispatch => bindActionCreators({ getResults }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search)
