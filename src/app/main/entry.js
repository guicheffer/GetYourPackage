/*-
 * ⭐️ MainEntry
 *
 * This is the main entry file for get-your-package spa ❤️.
 *
-*/

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Route, Switch, withRouter } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux'
import Button from 'react-toolbox/lib/button'
import { Helmet } from 'react-helmet'
import store, { history } from '../core/store'

import particles from '../styleguide/particles'
import { getTitle } from '../helpers/get-initialization-data'

import AbstractEntry from '../core/entry'

import Search from './pages/search'

// eslint-disable-next-line no-undef
const browser = window

const Welcome = withRouter(({ history: routerHistory }) => (
  <div className="panel__welcome">
    <Helmet>
      <title> {getTitle('Welcome')} </title>
    </Helmet>

    <Button
      raised primary
      className="welcome__button"
      label="Redirect me to the Search Page"
      onClick={() => { routerHistory.push('/search') }}
      to="/search"
      type="button"
    />
  </div>
))

class MainEntry extends AbstractEntry {
  start ({ initilizationData }) {
    this.initilizationData = initilizationData

    this.ui = {
      app: browser.document.querySelector('#app-get-your-package'),
    }

    this.render()
    this._customStart()
  }

  render () {
    ReactDOM.render(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <main className="get-your-package__panel">
            <Switch>
              <Route exact path="/" component={Welcome} />
              <Route exact path="/search/" component={Search} />

              <Route component={() => (
                <section className="panel__error" data-error="404">
                  <p> 4️⃣0️⃣4️⃣ 👉🏼 not found </p>

                  <Welcome/>

                  <Helmet>
                    <title> {getTitle('404')} </title>
                  </Helmet>
                </section>
              )} />
            </Switch>
          </main>
        </ConnectedRouter>
      </Provider>,
      this.ui.app,
    )
  }

  _customStart () {
    particles.init()
    this._giveInterviewer2NiceAndBeautifulGiantWelcomeMessages()
  }

  _giveInterviewer2NiceAndBeautifulGiantWelcomeMessages () {
    // eslint-disable-next-line no-console
    console.log(
      this.initilizationData.welcomeMessage.hi,
      'background: #333; color: #FFF; font-size: 12px; padding: 12px;',
      'font-size: 12px; font-style: italic;',
      'font-weight: bold; text-transform: uppercase;',
    )
    // eslint-disable-next-line no-console
    console.log(this.initilizationData.welcomeMessage.link, 'font-weight: bold;', '')
  }
}

export default MainEntry
