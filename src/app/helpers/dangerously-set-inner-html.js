/*-
 * ⭐️ Helper which helps react dangerouslySetInnerHTML native function
 *
-*/

export default string => string.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>')
